# Copyright 2009-2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'sbcl-1.0.41.ebuild' from Gentoo, which is:
# Copyright 1999-2010 Gentoo Foundation

SUMMARY="A high performance Common Lisp compiler"
HOMEPAGE="http://www.sbcl.org/"
DOWNLOADS="bootstrap? ( platform:amd64? ( mirror://sourceforge/${PN}/${PN}-1.2.3-x86-64-linux-binary.tar.bz2 ) )"

MYOPTIONS="
    bootstrap
    doc
    platform:
        amd64
"

LICENCES="MIT"
SLOT="1"

DEPENDENCIES="
    build:
        !bootstrap? (
            dev-lang/sbcl
        )
        doc? (
            sys-apps/texinfo
            media-gfx/graphviz
        )
    test:
        sys-apps/ed [[ note = [ for the test RUN-PROGRAM-ED ] ]]
"

src_prepare() {
    default

    expatch -p1 "${FILES}"/${PN}-1.0.49-skip_writeable_tests.patch
    expatch -p1 "${FILES}"/${PN}-sydbox.patch

    # https://bugs.launchpad.net/sbcl/+bug/626930
    edo sed -e "s:/tmp/:${TEMP}:" -i tests/load.impure.lisp

    # http://sbcl.cvs.sourceforge.net/sbcl/sbcl/doc/PACKAGING-SBCL.txt?view=markup
    edo sed -e "\$s:\"\$:-exherbo.${PR#r}\":" -i version.lisp-expr

    edo sed -e s:/bin/ed:/usr/$(exhost --target)/bin/ed: -i tests/run-program.impure.lisp

    # use prefixed nm
    edo sed -e "s/nm/$(exhost --tool-prefix)&/" -i src/runtime/linux-nm

    # Install the source code, e.g. for SLIME
    edo sed \
        -e '/cp $(MODULE).fasl/s:$(MODULE).fasl:$(MODULE).fasl $(MODULE).lisp:' \
        -i contrib/vanilla-module.mk
    edo tar cf source.tar src/
}

src_compile() {
    # For contrib/sb-bsd-sockets/tests.lisp
    esandbox allow_net --connect "inet:127.0.0.1@1024-65535"

    if option bootstrap; then
        local sbcl_bin_dir="$(echo ${WORKBASE}/${PN}-*-linux)"
        local sbcl_bin="${sbcl_bin_dir}/src/runtime/sbcl --core ${sbcl_bin_dir}/output/sbcl.core"
    else
        local sbcl_bin=/usr/$(exhost --target)/bin/sbcl
    fi
    local sbcl_args="--no-sysinit --no-userinit --disable-debugger"

    # make.sh needs a shell with a built-in "time" command
    edo bash make.sh --prefix=/usr --xc-host="${sbcl_bin} ${sbcl_args}"

    if option doc; then
        emake html info -C doc/internals
        emake html info -C doc/manual
    fi

    esandbox disallow_net --connect "inet:127.0.0.1@1024-65535"
}

src_test() {
    edo cd tests
    HOME=${HOME%/} edo sh run-tests.sh
}

src_install() {
    # SBCL_HOME conflicts with INSTALL_ROOT
    unset SBCL_HOME

    edo env \
        BUILD_ROOT="${IMAGE}" \
        DOC_DIR=/usr/share/doc/${PNVR} \
        INFO_DIR=/usr/share/info \
        MAN_DIR=/usr/share/man \
        INSTALL_ROOT=/usr/$(exhost --target) \
        sh install.sh

    local sbcl_source_root=/usr/src/${PN}
    dodir ${sbcl_source_root}
    edo tar --strip-components=1 -xf source.tar -C "${IMAGE}"${sbcl_source_root}/

    edo find "${IMAGE}"/usr/$(exhost --target)/lib -type d -empty -delete
    edo find "${IMAGE}"/usr/share -type d -empty -delete

    # Necessary for running newly-saved images
    hereenvd 50sbcl <<EOF
SBCL_HOME=/usr/$(exhost --target)/lib/${PN}
SBCL_SOURCE_ROOT=${sbcl_source_root}
EOF

    dodir /etc/
    edo cat > "${IMAGE}"/etc/sbclrc <<EOF
;;; The following is required if you want source location functions to
;;; work in SLIME, for example.

(setf (logical-pathname-translations "SYS")
    '(("SYS:SRC;**;*.*.*" #p"${sbcl_source_root}/**/*.*")))
EOF

    # Tell ASDF about our packages
    dodir /etc/common-lisp/
    edo cat > "${IMAGE}"/etc/common-lisp/source-registry.conf <<EOF
;;; -*- mode:common-lisp -*-
(:source-registry
 (:tree "/usr/share/common-lisp/source/")
 :inherit-configuration)
EOF
}

